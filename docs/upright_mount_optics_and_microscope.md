# Mount the optics and the microscope


{{BOM}}

[M3 nut]: parts/mechanical.yml#Nut_M3_SS
[2.5mm Ball-end Allen key]: parts/tools/2.5mmBallEndAllenKey.md

{{include: mount_microscope.md}}

## Mount the optics {pagestep}
* Take the [complete optics module](fromstep){qty:1, cat:subassembly} and [complete separate z-actuator](fromstep){qty:1, cat:subassembly} holding each upside down, side by side.
* Insert the exposed mounting screw on the optics module through the keyhole on the separate z-actuator.
* Insert the [2.5mm Ball-end Allen key]{qty:1, cat:tool, note: " Must be a ball-ended key"}  through the teardrop shaped hole on the back of the separate z-actuator, until it engages with the mounting screw.
* Slide optics module up the keyhole until it is **2-3mm** below the flat section of the bottom of the z-axis while keeping the Allen key engaged with the screw. 
* Tighten the screw with the Allen key to lock the optics in place. If the optics module will not reach the required place with the Allen key inserted, tighten the mounting screw with the Allen key engaged before firmly pushing the optics module into the required position. 

## Mounting the z-axis and z-actuator mount onto the main body {pagestep}
* Place the [Upright z-actuator mount](fromstep){qty:1, cat:printedpart} on top of the main body, triangular face of each touching one another.
* Place [M3x10 cap head screws](parts/mechanical.yml#CapScrew_M3x10mm_SS){qty: 3, cat:mech} into the two externally showing holes at the front of the main body and a third screw through the top of the vertical bored hole. Screw tightly into the nuts. 
* Place a [M3 nut]{qty:4, cat:mech} into each of the 4 nut traps in the z-actuator mount.
* Place the upside-down separate z-actuator with the optics module now attached onto the z-actuator mount. 
* While holding these together, place 4 [M3x10 cap head screws](parts/mechanical.yml#CapScrew_M3x10mm_SS){qty: 4, cat:mech}  into the 4 bored holes in the separate z-actuator and screw tightly into place. 
