# Print the holders for the motor control electronics

This page describes the parts to print if you are using three separate motor driver boards and an Arduino Nano microcontroller to drive your motors.

{{BOM}}

[PLA filament]: ../parts/materials/pla_filament.md "{cat:material}"
[RepRap-style printer]: ../parts/tools/rep-rap.md "{cat:tool}"

## Print the converter plate and board gripper {pagestep}

Using a [RepRap-style printer]{qty:1}, print the following parts using [PLA filament]{qty: 50 grams}.

* [nano_converter_plate-pi4.stl](../models/nano_converter_plate-pi4.stl){previewpage}  
* [nano_converter_plate_gripper.stl](../models/nano_converter_plate_gripper.stl){previewpage} (print in the side-on orientation as in the STL to avoid unsupported overhang)

>i The parts here fit a Raspberry Pi version 4. For parts to fit a version 3 see [customisations].

[customisations]: ../customisation.md

[nano converter plate]{output, qty:1, hidden}
[nano converter plate gripper]{output, qty:1, hidden}

