---
PartData:
    Specs:
        Viscosity: ISO 22
    Suppliers:
        McMaster Carr:
            PartNo: 1271K1
            Link: https://www.mcmaster.com/1271K1/
        RS Components:
            PartNo: 252-0168
            Link: https://uk.rs-online.com/web/p/oils/2520168/
---
# Light oil

Light oil is used to lubricate the microscope lead screw increasing life significantly. The microscope has been tested with 3-in-1 multi-purpose Oil with a ISO viscosity grade of 22. High quality oils of the same viscosity should perform similarly.