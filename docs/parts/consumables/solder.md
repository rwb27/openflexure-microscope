# Solder

Solder is required to make up the [LED + resistor work-around](../../workaround_5mm_led/workaround_5mm_led.md).  Any solder will do, provided you have a suitable soldering iron.