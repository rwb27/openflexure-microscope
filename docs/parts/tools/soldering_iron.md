# Soldering Iron

A soldering iron is required to make up the LED+resistor work-around, if an [illumination PCB](../electronics/illumination_pcb.md) is not available.  Any iron will do provided it is compatible with your chosen solder, but a relatively large chisel tip is probably best for wire-to-wire soldering.